#####################################
## NAVIGATION BREADCRUMB CONSTANTS ##
#####################################

plugin.tx_teufels_cpt_nav_breadcrumb {
	settings {
        lib {
            navigation {
                bBreadcrumb = 1
                sTreeLevel = 0,1
                class {
                    nav = breadcrumb-section hidden-xs hidden-sm
                    div = container
                }
                cache {
                    postfix = default
                }
            }
        }
        production {
            optional {
                active = 1
            }
        }
        production {
            includePath {
                public = EXT:teufels_cpt_nav_breadcrumb/Resources/Public/
                private = EXT:teufels_cpt_nav_breadcrumb/Resources/Private/
                frontend {
                    public = typo3conf/ext/teufels_cpt_nav_breadcrumb/Resources/Public/
                }

            }
            optional {
                active = 1
            }
        }
    }
}
